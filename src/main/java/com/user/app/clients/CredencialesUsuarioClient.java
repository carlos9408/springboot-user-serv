package com.user.app.clients;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.common.app.dto.rs.CredencialesUsuarioRS;

@FeignClient(value = "credentials-serv")
public interface CredencialesUsuarioClient {

	@GetMapping("/find/by/usuario/{idUsuario}")
	Optional<CredencialesUsuarioRS> getCredencialesByIdUsuario(@PathVariable(name = "idUsuario") final Long id);

}
