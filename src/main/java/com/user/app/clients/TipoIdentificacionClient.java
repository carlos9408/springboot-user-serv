package com.user.app.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.common.app.dto.rs.UsuarioRS;

@FeignClient(value = "tipo-identificacion-serv")
public interface TipoIdentificacionClient {

	/**
	 * Cliente Feign que se encarga de retormar la información de los Usuarios por
	 * su tipo de identifación
	 * 
	 * @param idTipoIdentificacion
	 * @return List<UsuarioRS> listado de usuarios en el DTO UsuarioRS
	 */
	@GetMapping("/find/usuarios/by/{idTipoIdentificacion}")
	public List<UsuarioRS> getUsuariosByTipoIdentificacion(
			@PathVariable(value = "idTipoIdentificacion") final Long idTipoIdentificacion);

}
