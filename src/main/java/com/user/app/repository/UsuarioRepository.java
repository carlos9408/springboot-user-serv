package com.user.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.common.app.entity.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	@Query(value = "SELECT u FROM Usuario u")
	List<Usuario> getAllRepo();

	@Query(value = "SELECT * FROM USUARIO", nativeQuery = true)
	List<Usuario> getAllNativo();

	@Modifying
	@Query("UPDATE Usuario u SET u.nombre =:nombre WHERE u.id=:id")
	void update(@Param("id") final Long id, @Param("nombre") final String nombre);

}
