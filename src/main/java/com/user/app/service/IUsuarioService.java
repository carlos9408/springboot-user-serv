package com.user.app.service;

import java.util.List;

import com.common.app.dto.rs.CredencialesUsuarioRS;
import com.common.app.dto.rs.UsuarioRS;
import com.common.app.entity.Usuario;

public interface IUsuarioService {

	List<Usuario> getAll();

	Usuario findById(final Long id);

	List<UsuarioRS> getUsuariosByTipoIdentificacion(final Long idTipoIdentificacion);

	CredencialesUsuarioRS getCredencialesByUsuarioId(final Long idUsuario);

}