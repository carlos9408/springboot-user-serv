package com.user.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.common.app.dto.rs.CredencialesUsuarioRS;
import com.common.app.dto.rs.UsuarioRS;
import com.common.app.entity.Usuario;
import com.common.app.util.Util;
import com.user.app.clients.CredencialesUsuarioClient;
import com.user.app.clients.TipoIdentificacionClient;
import com.user.app.repository.UsuarioRepository;
import com.user.app.service.IUsuarioService;

import brave.Tracer;

@Service
@Transactional
@Primary
public class IUsuarioServicePrimaryImpl implements IUsuarioService {

	@Autowired
	private static final Logger LOGGER = LoggerFactory.getLogger(IUsuarioServiceAuxiliarImpl.class);

	@Autowired
	private UsuarioRepository usuarioRepo;

	@Autowired
	private TipoIdentificacionClient tipoIdentificacionFeign;

	@Autowired
	private CredencialesUsuarioClient credencialesUsuarioFeign;

	@Autowired
	private Environment env;

	@Autowired
	private Tracer tracer;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> getAll() {
		LOGGER.info("Consulta de usuarios en Service primary");
		final List<Usuario> result = usuarioRepo.getAllRepo();
		LOGGER.info(env.getProperty(Util.SERVER_PORT));
		result.forEach(res -> res.setPort(env.getProperty(Util.SERVER_PORT)));

		tracer.currentSpan().tag("local.port", env.getProperty(Util.SERVER_PORT));
		tracer.currentSpan().tag("remote.port", "NA");

		return result;
	}

	@Override
	public Usuario findById(Long id) {
		LOGGER.info("Consulta de usuario by id en Service primary");
		final Optional<Usuario> usuario = usuarioRepo.findById(id);
		if (usuario.isPresent()) {
			return usuario.get();
		}
		throw new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND);
	}

	@Override
	@Transactional(readOnly = true)
	public List<UsuarioRS> getUsuariosByTipoIdentificacion(Long idTipoIdentificacion) {
		final List<UsuarioRS> response = tipoIdentificacionFeign.getUsuariosByTipoIdentificacion(idTipoIdentificacion);
		response.forEach(rs -> rs.setRemotePort(env.getProperty(Util.SERVER_PORT)));
		return response;
	}

	@Override
	@Transactional(readOnly = true)
	public CredencialesUsuarioRS getCredencialesByUsuarioId(final Long idUsuario) {
		return credencialesUsuarioFeign.getCredencialesByIdUsuario(idUsuario).map(rs -> {
			rs.setRemotePort(env.getProperty(Util.SERVER_PORT));
			return rs;
		}).orElseThrow(() -> new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND));

	}
}
