package com.user.app.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.common.app.dto.rs.CredencialesUsuarioRS;
import com.common.app.dto.rs.UsuarioRS;
import com.common.app.entity.Usuario;
import com.common.app.util.Util;
import com.user.app.clients.CredencialesUsuarioClient;
import com.user.app.clients.TipoIdentificacionClient;
import com.user.app.service.IUsuarioService;

@Service
@Transactional
@Qualifier(value = "usuarioServiceAuxiliar")
public class IUsuarioServiceAuxiliarImpl implements IUsuarioService {

	@Autowired
	private static final Logger LOGGER = LoggerFactory.getLogger(IUsuarioServiceAuxiliarImpl.class);

	@Autowired
	private EntityManager em;

	@Autowired
	private TipoIdentificacionClient tipoIdentificacionFeign;

	@Autowired
	private CredencialesUsuarioClient credencialesUsuarioFeign;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> getAll() {
		LOGGER.info("Consulta de usuarios en Service auxiliar");
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<Usuario> cr = cb.createQuery(Usuario.class);
		final Root<Usuario> root = cr.from(Usuario.class);
		cr.select(root);
		return em.createQuery(cr).getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findById(Long id) {
		LOGGER.info("Consulta de usuario by id en Service auxiliar");
		final Usuario usuario = em.find(Usuario.class, id);
		if (null != usuario) {
			return usuario;
		} else {
			throw new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public List<UsuarioRS> getUsuariosByTipoIdentificacion(final Long idTipoIdentificacion) {
		return tipoIdentificacionFeign.getUsuariosByTipoIdentificacion(idTipoIdentificacion);
	}

	@Override
	@Transactional(readOnly = true)
	public CredencialesUsuarioRS getCredencialesByUsuarioId(final Long idUsuario) {
		return credencialesUsuarioFeign.getCredencialesByIdUsuario(idUsuario)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.OK, Util.NO_RESULTS_FOUND));
	}
}
