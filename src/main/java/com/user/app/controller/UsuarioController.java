package com.user.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.common.app.dto.rs.CredencialesUsuarioRS;
import com.common.app.dto.rs.UsuarioRS;
import com.common.app.entity.Usuario;
import com.user.app.service.IUsuarioService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping
public class UsuarioController {

	@Autowired
	private IUsuarioService usuarioServicePrimary;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	@Qualifier("usuarioServiceAuxiliar")
	private IUsuarioService usuarioServiceAuxiliar;

	@GetMapping("/primary/all")
	public List<Usuario> getAllPrimary() {
		return usuarioServicePrimary.getAll();
	}

	@GetMapping("/primary/find/{id}")
	public Usuario findByIdPrimary(@PathVariable(name = "id") final Long id) {
		return usuarioServicePrimary.findById(id);
	}

	@GetMapping("/auxiliar/all")
	public List<Usuario> getAllAuxiliar() {
		return usuarioServiceAuxiliar.getAll();
	}

	@GetMapping("/auxiliar/find/{id}")
	public Usuario findByIdAuxiliar(@PathVariable(name = "id") final Long id) {
		return usuarioServiceAuxiliar.findById(id);
	}

	@GetMapping("/find/by/tipoId/{idTipoIdentificacion}")
	public List<UsuarioRS> findByTipoIdentificacion(
			@PathVariable(name = "idTipoIdentificacion") final Long idTipoIdentificacion) {
		return usuarioServicePrimary.getUsuariosByTipoIdentificacion(idTipoIdentificacion);
	}

	@GetMapping("/find/credenciales/{idUsuario}")
	public CredencialesUsuarioRS findCredencialesByUsuario(@PathVariable(name = "idUsuario") final Long idUsuario) {
		return usuarioServicePrimary.getCredencialesByUsuarioId(idUsuario);
	}

	@GetMapping("/find/error")
	@CircuitBreaker(name = "userService", fallbackMethod = "getErrorFallback")
	public ResponseEntity<String> getGeneralError() {
		final String res = restTemplate.getForObject("http://localhost:8081/find", String.class);
		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	public ResponseEntity<String> getErrorFallback(Exception e) {
		return new ResponseEntity<>("El servicio presenta intermitencia, intente mas tarde", HttpStatus.OK);
	}

	public ResponseEntity<String> getErrorFallbackIO(Exception e) {
		return new ResponseEntity<>("Se presentó un error de java, intente mas tarde", HttpStatus.OK);
	}
}